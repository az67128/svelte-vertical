import { writable, derived } from "svelte/store";
import {FILMS} from './constants'

export const markup = writable({});
export const frameRate = writable(24);
export const filmId = writable(null);
export const filmEpisodeId = writable(null);
export const quality = writable(null)
export const video = writable({});

export const film = derived(filmId, ($filmId)=>{
  const filmItem = FILMS.find(item=>item.id ===$filmId)
  
  return filmItem || {};
})

export const episode = derived([film, filmEpisodeId], ([$film, $filmEpisodeId])=>{
  if (!$film || !$film.episodes) return {}
  const episodeItem = $film.episodes.find(item=>item.id === $filmEpisodeId)
  return episodeItem || {}
})

export const filmSources = derived(film, ($film)=>{
  return $film.filmUrls || [];
})

export const videoQualityList = derived(filmSources, ($filmSources) => {
  if (!$filmSources) return [];
  return Object.keys($filmSources).sort((a, b) => a - b);
});
videoQualityList.subscribe(state=>{
  quality.set(state.length > 2 ? state.slice(-3, -2) : state[0]);
})

export const selectedEpisode = writable(null);

export const paused = writable(true)

