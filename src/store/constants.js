export const IS_TEST = /(localhost|192\.168|az67128.gitlab.io)/.test(
  window.location.href
);
export const IS_VK = /vk_app_id/.test(window.location.href);
export const IS_MOBILE_VK = /vk_platform\=mobile/.test(window.location.href);

export const BASE_URL = /(localhost|192\.168)/.test(window.location.href)
  ? "."
  : ".";
// : "https://az67128.gitlab.io/svelte-vertical";

export const FILMS = [
  {
    id: 8,
    cover: "elkilast.png",
    title: "Ёлки Последние",
    description: "2018, Россия, Комедии, Русские",
    kinopisk: "5.6",
    filmUrl:
      "https://ongocinemamedia-euno.streaming.media.azure.net//f41a7fab-b88f-49b2-a961-6518a3a73d28/elki-7-1080-2.ism/manifest(format=m3u8-aapl,encryption=cbc)",
    height: 800,
    episodes: [
      {
        id: 1,
        title: "Серия 1",
        markup: "/data/elki7/YOLKI_7_1_VERTICAL-24fps-1080x2340.vxml",
        start: 0,
        end: 15381,
        cover: "/elki7/7_series_1.jpg",
      },
      {
        id: 2,
        title: "Серия 2",
        markup: "/data/elki7/YOLKI_7_2_VERTICAL-24fps-1080x2340.vxml",
        start: 15383,
        end: 31116,
        cover: "/elki7/7_series_2.jpg",
      },
      {
        id: 3,
        title: "Серия 3",
        markup: "/data/elki7/YOLKI_7_3_VERTICAL-24fps-1080x2340.vxml",
        start: 31118,
        end: 51441,
        cover: "/elki7/7_series_3.jpg",
      },
      {
        id: 4,
        title: "Серия 4",
        markup: "/data/elki7/YOLKI_7_4_VERTICAL-24fps-1080x2340.vxml",
        start: 51445,
        end: 68170,
        cover: "/elki7/7_series_4.jpg",
      },
      {
        id: 5,
        title: "Серия 5",
        markup: "/data/elki7/YOLKI_7_5_VERTICAL-24fps-1080x2340.vxml",
        start: 68172,
        end: 82373,
        cover: "/elki7/7_series_5.jpg",
      },
      {
        id: 6,
        title: "Серия 6",
        markup: "/data/elki7/YOLKI_7_6_VERTICAL-24fps-1080x2340.vxml",
        start: 82375,
        end: 96967,
        cover: "/elki7/7_series_6.jpg",
      },
      {
        id: 7,
        title: "Серия 7",
        markup: "/data/elki7/YOLKI_7_7_VERTICAL-24fps-1080x2340.vxml",
        start: 96969,
        end: 117100,
        cover: "/elki7/7_series_7.jpg",
      },
      {
        id: 8,
        title: "Серия 8",
        markup: "/data/elki7/YOLKI_7_8_VERTICAL-24fps-1080x2340.vxml",
        start: 117107,
        end: 132027,
        cover: "/elki7/7_series_8.jpg",
      },
    ],
    markupUrl: "/data/elkilast.vxml",
    year: 2018,
  },
  // {
  //   id: 7,
  //   cover: "elkinovie.png",
  //   title: "Ёлки новые",
  //   description: "2017, Россия, Комедии, Русские",
  //   kinopisk: "5.8",
  //   filmUrl: 'https://ongocinemamedia-euno.streaming.media.azure.net//a6ba751f-f6c2-4b27-bd84-ea0fec604cc7/YOLKI-6-24fps-ORIGINAL-1920x800-.ism/manifest(format=m3u8-aapl,encryption=cbc)',
  //   height: 800,
  //   episodes: [
  //     {
  //       id: 1,
  //       title: "Серия 1",
  //       markup: "/data/elkinovie.vxml",
  //       start: 0,
  //       end: 132027,
  //       cover: "elkinovie.png",
  //     },
  //   ],
  //   markupUrl: "/data/elkinovie.vxml",
  //   year: 2017,
  // },
  {
    id: 6,
    cover: "elki5.png",
    title: "Ёлки 5",
    description: "2016, Россия, Комедии, Русские",
    kinopisk: "4.8",
    filmUrl: 'https://ongocinemamedia-euno.streaming.media.azure.net//466706a0-21b6-40b9-980d-4c4250330da5/YOLKI-5-24fps-ORIGINAL-1920x800-.ism/manifest(format=m3u8-aapl,encryption=cbc)',
    height: 800,
    episodes: [
      {
        id: 1,
        title: "Серия 1",
        markup: "/data/elki5.vxml",
        start: 0,
        end: 120723,
        cover: "elki5.png",
      },
    ],
    markupUrl: "/data/elki5.vxml",
    year: 2016,
  },
  {
    id: 5,
    cover: "elki1914.png",
    title: "Ёлки 1914",
    description: "2014, Россия, Комедии, Русские",
    kinopisk: "5.7",
    filmUrl: 'https://ongocinemamedia-euno.streaming.media.azure.net//0a1fce2a-1273-4844-9b22-db9c5c54438e/YOLKI_1914-24fps-ORIGINAL-1920x8.ism/manifest(format=m3u8-aapl,encryption=cbc)',
    height: 800,
    episodes: [
      {
        id: 1,
        title: "Серия 1",
        markup: "/data/elki1914.vxml",
        start: 0,
        end: 148324,
        cover: "elki1914.png",
      },
    ],
    markupUrl: "/data/elki1914.vxml",
    year: 2014,
  },
  // {
  //   id: 4,
  //   cover: "elkilohmatie.png",
  //   title: "Ёлки лохматые",
  //   description: "2014, Россия, Комедии, Русские",
  //   kinopisk: "5.0",
  //   filmUrl:
  //     "https://ongocinemamedia-euno.streaming.media.azure.net//4e69b689-de10-4ccb-bdfb-119c782557ec/YOLKI_LOHMATIE-24fps-ORIGINAL-19.ism/manifest(format=mpd-time-csf,encryption=cbc)",
  //   episodes: [
  //     {
  //       id: 1,
  //       title: "Серия 1",
  //       markup: "/data/elkilohmatie.vxml",
  //       start: 0,
  //       end: 118506,
  //       cover: "elkilohmatie.png",
  //     },
  //   ],
  //   markupUrl: "/data/elkilohmatie.vxml",
  //   year: 2014,
  // },
  // {
  //   id: 3,
  //   cover: "elki3.png",
  //   title: "Ёлки 3",
  //   description: "2013, Россия, Комедии, Русские",
  //   kinopisk: "6.5",
  //   filmUrl:"https://ongocinemamedia-euno.streaming.media.azure.net//bf5e7886-1b7c-47e7-b796-00f871d078a7/YOLKI-3_25fps-ORIGINAL-1920x800p.ism/manifest(format=m3u8-aapl,encryption=cbc)",
  //   episodes: [
  //     {
  //       id: 1,
  //       title: "Серия 1",
  //       markup: "/data/VERTICAL.vxml",
  //       start: 0,
  //       end: 144317,
  //       cover: "elki3.png",
  //     },
  //   ],
  //   markupUrl: "/data/VERTICAL.vxml",
  //   year: 2013,
  // },
  {
    id: 2,
    cover: "elki2.png",
    title: "Ёлки 2",
    description: "2011, Россия, Комедии, Русские",
    kinopisk: "7.0",
    filmUrl:
      "https://ongocinemamedia-euno.streaming.media.azure.net//15744a37-e2e2-4ece-b297-25595da93b03/YOLKI-2-25fps-ORIGINAL-1920x800-.ism/manifest(format=m3u8-aapl,encryption=cbc)",
    episodes: [
      {
        id: 1,
        title: "Серия 1",
        markup: "/data/elki2.vxml",
        start: 0,
        end: 144317,
        cover: "elki2.png",
      },
    ],
    markupUrl: "/data/elki2.vxml",
    year: 2011,
  },
  // {
  //   id: 1,
  //   cover: "elki.png",
  //   title: "Ёлки",
  //   description: "2010, Россия, Комедии, Русские",
  //   kinopisk: "6.9",
  //   filmUrl: "https://ongocinemamedia-euno.streaming.media.azure.net//39dbb93c-1f60-4f84-9f08-7057b81200b9/YOLKI-1_ORIGINAL_25fps_1920x800p.ism/manifest(format=m3u8-aapl,encryption=cbc)",
  //   episodes: [
  //     {
  //       id: 1,
  //       title: "Серия 1",
  //       markup: "/data/VERTICAL.vxml",
  //       start:0,
  //       end:15381,
  //       cover:'/elki7/7_series_1.jpg'
  //     },
  //   ],
  //   markupUrl: "/data/VERTICAL.vxml",
  //   year: 2010,
  // },
  
];

export const SERIES = [
  {
    cover: "episode1.png",
    title: "Боря пытается обнулить Новый год",
    subtitle: "1 серия",
    url: "https://www.youtube.com/embed/rZuFo84U7J0",
  },
  {
    cover: "episode2.png",
    title: "Баба Маня пропала",
    subtitle: "2 серия",
    url: "https://www.youtube.com/embed/2VKv0HL0ZaQ",
  },
  {
    cover: "episode3.png",
    title: "Назови меня моим именем",
    subtitle: "3 серия",
    url: "https://www.youtube.com/embed/dGRPAH6hfX0",
  },
  {
    title: "Отцы и дети",
    subtitle: "4 серия",
    url: "https://www.youtube.com/embed/iAktE2lepGY",
  },
  {
    title: "Рай в шалаше",
    subtitle: "5 серия",
    url: "https://www.youtube.com/embed/N4Q3X0cxM1k",
  },
  {
    title: "Лучше рано, чем никогда",
    subtitle: "6 серия",
    url: "https://www.youtube.com/embed/U-0o16OnzsQ",
  },
  {
    title: "Конец света",
    subtitle: "7 серия",
    url: "https://www.youtube.com/embed/iwPDna21Psg",
  },
  {
    title: "С любимыми не расставайтесь",
    subtitle: "8 серия",
    url: "https://www.youtube.com/embed/Ih1rhM57mh4",
  },
  {
    title: "Дина и мина",
    subtitle: "9 серия",
    url: "https://www.youtube.com/embed/WCisvOGPAc8",
  },
  {
    title: "На Деда Мороза надейся, а сам не плошай",
    subtitle: "10 серия",
    url: "https://www.youtube.com/embed/cAAUuSO2vdA",
  },
];
