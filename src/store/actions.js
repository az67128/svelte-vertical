import {
  markup,
  frameRate,
  video,
  selectedEpisode,
  filmId,
  filmEpisodeId,
  film,
} from "./index";
import { router } from "tinro";
import { get } from "svelte/store";
import loadMarkup from "../helpers/loadMarkup";
import { BASE_URL, FILMS } from "./constants";
import sendMetrik from "../helpers/sendMetrik";
import { spring } from "svelte/motion";
import bridge from "@vkontakte/vk-bridge";


export const startPlayer = ({ id, episodeId }) => {
  const film = FILMS.find((item) => item.id === id);
  if (!film) return;
  markup.set({});
  const episode = film.episodes.find((item) => item.id === episodeId);
  filmId.set(id);
  filmEpisodeId.set(episodeId);

  sendMetrik("FilmStart", { FilmName: film.title, filmEpisode: episode.title });
  loadMarkup(BASE_URL + episode.markup).then((res) => {
    markup.set(res.markup);
    frameRate.set(res.frameRate);
    get(video).play();
  });
};
export const goToMain = () => {
  const currentFilm = get(film);
  if(currentFilm.episodes.length === 1){
    router.goto('/')
  } else {
    router.goto('/film/' + currentFilm.id)
  }
  
};

export const selectEpisode = (episode) => {
  
  sendMetrik("EpisodeStart", { EpisodeNumber: episode.subtitle });
  selectedEpisode.set(episode);
};

export const resetEpisode = () => {
  selectedEpisode.set(null);
};

let isPanStarted = false;
export const coords = spring(
  { x: 0, y: 0 },
  {
    stiffness: 0.2,
    damping: 0.4,
  }
);

export function handlePanStart() {
  coords.stiffness = coords.damping = 1;
  isPanStarted = true;
}

export function handlePanMove(event, seeking) {
  if (seeking) return;
  if (window.innerWidth > window.innerHeight) return;
  if (isPanStarted) {
    sendMetrik("FilmPullFrame", {
      FilmPullFrameId: Math.floor(get(video).currentTime * get(frameRate)),
    });
    isPanStarted = false;
  }
  coords.update(($coords) => ({
    x: $coords.x + event.detail.dx * 1.5,
    y: $coords.y + event.detail.dy * 1.5,
  }));
}

export function handlePanEnd(event) {
  coords.stiffness = 0.2;
  coords.damping = 0.4;
  coords.set({ x: 0, y: 0 });
}

export const onVisibilityChange = () => {
  if (document.visibilityState === "hidden") {
    const videoElement = get(video)
    if (videoElement && videoElement.pause) videoElement.pause();
  }
};

export const initVk = () => {
  bridge.send("VKWebAppInit", {}).then(() => {
    return bridge.send("VKWebAppGetUserInfo", {});
  });
  bridge.subscribe((event) => {
    if (event.detail && event.detail.type === "VKWebAppViewHide") {
      const videoElement = get(video)
      if (videoElement && videoElement.pause) videoElement.pause();
    }
  });
};
