 const loadMarkup = async (url) => {
    const markup = {}
    let min = 9999;
    let max = 0;
    const xml = await fetch(url).then((res) => res.text());
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(xml, "application/xml");
    const frameRateEl = xmlDoc.querySelector('VideoParameter[name="fps"]')
    const frameRate = frameRateEl ? Number(frameRateEl.getAttribute('data')) : 24;
  
    [...xmlDoc.getElementsByTagName("Effect")].forEach((effect) => {
      const frame = Number(effect.getAttribute("frame"));
      if (frame < min) min = frame
      if (frame > max) max = frame
      if (!markup[frame]) markup[frame] = {};
      const actions = effect.getAttribute("action");
      const type = effect.getAttribute("type");
  
      if (actions === "static") {
        if (markup[frame][type] && frame === 0) {
          // effect exists
        } else if (type === "position") {
          markup[frame][type] = effect.getAttribute("value").split(",").map(Number);
        } else {
          markup[frame][type] = Number(effect.getAttribute("value"));
        }
      }
      if (actions === "animated") {
        if (type === "position") {
          const startFrame = Number(effect.getAttribute("start"));
          const endFrame = Number(effect.getAttribute("end"));
  
          const animationKeys = [];
  
          [...effect.getElementsByTagName("key")].forEach((key, i) => {
            const keyFrame = key.getAttribute("frame");
            const value = key.getAttribute("value").split(",").map(Number);
            animationKeys.push({ frame: Number(keyFrame), value });
          });
          //find first frame for animation
          if (animationKeys[0].frame > startFrame) {
            const prevStaticEl = xmlDoc.querySelector(
              `Effect[action="static"][type="position"][frame="${startFrame}"]`
            );
            const prevAnimatedKeyEl = xmlDoc.querySelector(
              `Effect[action="animated"][type="position"][start="${startFrame}"] key:last-child`
            );
            if (prevStaticEl) {
              const value = prevStaticEl.getAttribute("value").split(",").map(Number);
              animationKeys.unshift({ frame: endFrame, value });
            } else if (prevAnimatedKeyEl) {
              const value = prevAnimatedKeyEl.getAttribute("value").split(",").map(Number);
              animationKeys.unshift({ frame: endFrame, value });
            } else {
              console.log('oops, we have a problem')
            }
          }
          //find first frame for animation
          let leftIndex = 0;
          let rightIndex = 1;
          
          for (let i = startFrame; i < endFrame; i++) {
            if(animationKeys[rightIndex] && animationKeys[rightIndex].frame===i){
              leftIndex++;
              rightIndex++;
            }
            if (!markup[i]) markup[i] = {};
            const leftKey = animationKeys[leftIndex]
            const rightKey = animationKeys[rightIndex]
            if (rightKey){
              markup[i][type] = [
                leftKey.value[0]+ ((i-leftKey.frame)/(rightKey.frame-leftKey.frame))*(rightKey.value[0] - leftKey.value[0]),
                leftKey.value[1]+ ((i-leftKey.frame)/(rightKey.frame-leftKey.frame))*(rightKey.value[1] - leftKey.value[1]),
                ...(i>startFrame  ? ['animated'] :[])
              ]
            } else {
              markup[i][type] = [...leftKey.value]
            }
            if (i < min) min = i
            if (i > max) max = i
          }
        }
      }
    });
    for (let i = min; i<=max; i++){
      if(! markup[i-1]) continue
      if(!markup[i]) markup[i] = {}
      if (!markup[i].position && markup[i-1].position) markup[i].position = markup[i-1].position
      if (!markup[i].scale && markup[i-1].scale) markup[i].scale = markup[i-1].scale
      if (!markup[i].rotation && markup[i-1].rotation) markup[i].rotation = markup[i-1].rotation
    }
    return {markup, frameRate};
  };
  
  export default loadMarkup